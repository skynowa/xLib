xLib - C++ cross-platform library


Authors: skynowa
E-mail:  skynowa@gmail.com
Skype:   skynowa777
Jabber:  skynowa@jabber.ru
ICQ:     627713628
Web:     https://bitbucket.org/skynowa/xlib
         https://github.com/skynowa/xLib
         https://sourceforge.net/p/x-lib

Features:
    - platforms:
        Windows:
            + Windows (XP, Vista, Seven)
            - Windows CE
        Unix:
            BSD:
                - DragonFly BSD
                + FreeBSD
                - OpenBSD
                - NetBSD
            Linux:
                + Centos
                + Debian
                + Fedora
                + OpenSUSE
                + RedHat
                + Ubuntu
                + Android
            Apple:
                + OSX
                - iOS
                + Darwin
            Solaris:
                - Oracle Solaris
                - Open Indiana

    - architecture: x86, x64
    - compilers: MinGW, MSVC, CodeGear, Clang, GCC
    - header only
    - unicode support

Repositories:
    - https://code.google.com
        git clone https://skynowa@code.google.com/p/xlib-library/

    - http://bitbucket.org
        git clone https://skynowa@bitbucket.org/skynowa/xlib.git
        git clone git@bitbucket.org:skynowa/xlib.git

    - http://github.com
        git clone https://github.com/skynowa/xLib.git
        git clone git://github.com/skynowa/xLib.git

    - http://sourceforge.net
        git clone https://skynowa@git.code.sf.net/p/x-lib/code x-lib-code
        git clone git://git.code.sf.net/p/x-lib/code x-lib-code
